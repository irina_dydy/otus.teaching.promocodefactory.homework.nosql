﻿using System;

namespace Otus.Teaching.Pcf.Redis.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}